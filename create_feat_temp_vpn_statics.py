"""
Author: Mario Uriel Romero
Organization: Cisco CX Americas
Purpose: Create a feature template with optional static routes (device specific)

"""


import json
from jinja2 import Template
from authentication import *

def create_template(jsessionid,token,base_url,source_file):
    if token is not None:
        header = {'Content-Type': "application/json", 'Cookie': jsessionid, 'X-XSRF-TOKEN': token}
    else:
        header = {'Content-Type': "application/json", 'Cookie': jsessionid}
    
    template_url = "dataservice/template/feature"
   
    with open(source_file,'r') as jinf:
        jinjapayload=Template(jinf.read())
    
    jsonpayload = jinjapayload.render()



    response=requests.post(url=f"{base_url}{template_url}", headers=header,data=jsonpayload,verify=False)
    if response.status_code==200:
        print("Template created succesfully")
    else:
        print("Error trying to create template ",response.status_code,response.text)


if __name__ == "__main__":
    base_url="https://X.X.X.X:8443/"
    source_file="vpn10_payload.j2"
    jsessionid=get_jsessionid(base_url)
    token=get_token(jsessionid,base_url)
    create_template(jsessionid,token,base_url,source_file)